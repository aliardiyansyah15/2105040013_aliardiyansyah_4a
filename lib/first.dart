import 'package:flutter/material.dart';
import 'package:login_flutter/login.dart';

class awal extends StatefulWidget {
  const awal({super.key});

  @override
  State<awal> createState() => _awalState();
}

class _awalState extends State<awal> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Image(
          image: AssetImage(
            "assets/images/Group_5-removebg-preview.png",
          ),
          width: 60.0,
          height: 100.0,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(25.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: new Image(
                image: AssetImage(
                  "assets/images/Group_5-removebg-preview.png",
                ),
                width: 300.0,
                height: 250.0,
              ),
            ),
            Center(
              child: SizedBox(
                width: 405.0,
                height: 50.0,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => loginpage(),
                      ),
                    );
                  },
                  child: Text(
                    "Mulai",
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
